﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UtilityServices.Models
{
    public class FolderInfo
    {
        public string InputPath { get; set; }
        public string OutputPath { get; set; }
        public string Nas1InputPath { get; set; }
        public string Nas1OutputPath { get; set; }
        public string Nas2InputPath { get; set; }
        public string Nas2OutputPath { get; set; }
        public string Nas3InputPath { get; set; }
        public string Nas3OutputPath { get; set; }
        public long InstId { get; set; }
        public long ImageId { get; set; }
        public long StudyId { get; set; }
        public string Threshold { get; set; }
        public string Age { get; set; }
    }
}
