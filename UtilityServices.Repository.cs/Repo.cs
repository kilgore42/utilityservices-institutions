﻿using Dapper;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityServices.Models;

namespace UtilityServices.Repository
{
    public class Repo
    {
        public String ConnectionString { get; set; }
        public Repo(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        public List<PurgeConfiguration> GetPurgeConfiguration()
        {
            List<PurgeConfiguration> config = new List<PurgeConfiguration>();
            PurgeConfiguration configuration = new PurgeConfiguration();
            using (IDbConnection db = new MySqlConnection(this.ConnectionString))
            {
                string SqlString = string.Format("SELECT Id, Name, Threshold FROM data_purge_config");
                config = (List<PurgeConfiguration>)db.Query<PurgeConfiguration>(SqlString);
            }
            return config;
        }

        public List<FolderInfo> GetFoldersToDelete(int limit, long maxid)
        {
            List<FolderInfo> config = new List<FolderInfo>();
            string maxIdStr = string.Format(" AND img.id > {0}", maxid);

            using (IDbConnection db = new MySqlConnection(this.ConnectionString))
            {
                string SqlString = @"select distinct(img.path_name) as InputPath, img.study_id as StudyId, ifnull(cfg.institution_id, 0) as InstId, ifnull(cfg.threshold, 0) as Threshold,
DATEDIFF(CURDATE(), s.incoming_timestamp) as Age, img.Id as ImageId, s.study_id as OutputPath FROM core_study s
inner join core_image img on s.id = img.study_id
inner join core_doctor d on s.doctor_id = d.id
left outer join institution_purge_config cfg on d.institution_id = cfg.institution_id
inner join core_institution i on d.institution_id = i.id 
WHERE s.AreFoldersDeleted = 0" + maxIdStr + " AND s.id >= 118 and img.path_name is not null AND ifnull(cfg.threshold, 0) > 0 AND DATEDIFF(CURDATE(), s.incoming_timestamp) > ifnull(cfg.threshold, 0) ORDER BY img.id, img.study_id limit " + limit;

                config = (List<FolderInfo>)db.Query<FolderInfo>(SqlString);
            }
            return config;
        }

        public List<FolderInfo> GetFoldersToMove(int limit, long maxid, string institution)
        {
            //The institution is hard coded for Caremax

            List<FolderInfo> config = new List<FolderInfo>();
            string maxIdStr = string.Format(" AND img.id > {0}", maxid);

            using (IDbConnection db = new MySqlConnection(this.ConnectionString))
            {
                //                string SqlString = @"select distinct(img.path_name) As InputPath FROM core_study s 
                //inner join core_image img on s.id = img.study_id 
                //inner join core_doctor d on s.doctor_id = d.id
                //inner join core_institution i on d.institution_id = i.id 
                //WHERE img.id > 0 AND s.id >= 118 and img.path_name is not null AND i.id = 233 AND img.path_name like '%202102%'
                //ORDER BY img.path_name limit 5000";

                string SqlString = @"select distinct(img.path_name) as InputPath, img.study_id as StudyId FROM core_study s
inner join core_image img on s.id = img.study_id
WHERE s.operators_name like 'TP' AND img.id > 0 AND s.id >= 118 and img.path_name is not null AND(img.path_name like '%202103%' or img.path_name like '%202104%');";



                config = (List<FolderInfo>)db.Query<FolderInfo>(SqlString);
            }
            return config;
        }

        public List<RowItem> GetdBItemsToDelete(string tableName, int threshold, int limit )
        {
            List<RowItem> items = new List<RowItem>();
            using (IDbConnection db = new MySqlConnection(this.ConnectionString))
            {
                db.Open();
                string SqlString = string.Format("SELECT Id, DateCreated FROM {0} WHERE DateCreated  IS NOT NULL AND DATEDIFF(CURDATE(), DateCreated) > {1} ORDER BY ID LIMIT {2}", tableName, threshold, limit);
                items = (List<RowItem>)db.Query<RowItem>(SqlString);
                db.Close();
            }

            return items;
        }

        public void DeleteItems(string tableName, List<long> ids)
        {
            using (IDbConnection db = new MySqlConnection(this.ConnectionString))
            {
                db.Open();
                string SqlString = string.Format("DELETE FROM {0} WHERE Id IN ({1})", tableName, string.Join(",",ids.ToArray()));
                db.Execute(SqlString);
                db.Close();
            }
        }

        public void AddAuditRecord(long instId, string imgName, long imgSize, int isNas)
        {
            using (IDbConnection db = new MySqlConnection(this.ConnectionString))
            {
                db.Open();
                string SqlString = string.Format("insert into institution_purge_audit (institution_id,image_name, image_size,is_nas) values({0},'{1}',{2},{3})", instId, imgName, imgSize,isNas);
                db.Execute(SqlString);
                db.Close();
            }
        }
        public void UpdateStudyFinished(long studyId)
        {
            using (IDbConnection db = new MySqlConnection(this.ConnectionString))
            {
                db.Open();
                string SqlString = string.Format("update core_study set AreFoldersDeleted = 1 where id = {0}", studyId);
                db.Execute(SqlString);
                db.Close();
            }
        }

        public void UpdateOcrQueue()
        {
            using (IDbConnection db = new MySqlConnection(this.ConnectionString))
            {
                db.Open();
                string SqlString = "update ocrqueue set status = 1 where datecreated > '2018-07-07' and DATEDIFF(CURDATE(), DateCreated) > 1 and status = 2";
                db.Execute(SqlString);
                db.Close();
            }

        }

        public void OptimizeTable(string tableName)
        {
            using (IDbConnection db = new MySqlConnection(this.ConnectionString))
            {
                db.Open();
                string SqlString = string.Format("OPTIMIZE TABLE {0}", tableName);
                db.Execute(SqlString);
                db.Close();
            }
        }

    }
}
