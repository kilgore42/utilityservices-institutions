﻿using NLog;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityServices.Models;
using UtilityServices.Repository;

namespace UtilityServices.Business.Plugins
{
    [Export(typeof(IPlugin))]
     public class DataPurge : IPlugin
    {
        public bool IsProcessing { get; set; }
        public TimeSpan Frequency { get; set; }
        public DateTime NextPurge { get; set; }
        private Repo Repo { get; set; }
        public string Name { get; set; }
        public NLog.Logger Logger { get; set; }
        public bool Enabled { get; set; }
        public DataPurge()
        {
            
        }
        public void Initialize(string connectionString, NameValueCollection configuration, Logger logger)
        {
            IsProcessing = false;
            this.Repo = new Repo(connectionString);
            string purgeFrequency = configuration["DataPurgeFrequency"];
            if(string.IsNullOrEmpty(purgeFrequency))
            {
                purgeFrequency = "1:00:00:00";
            }
            if (string.IsNullOrEmpty(purgeFrequency))
            {
                purgeFrequency = "1:00:00:00";
            }

            this.Frequency = TimeSpan.Parse(purgeFrequency);
            NextPurge = DateTime.UtcNow;

            string enabled = configuration["DataPurgeEnabled"];
            if(!string.IsNullOrEmpty(enabled))
            {
                this.Enabled = Convert.ToBoolean(enabled);
            }
            
            
            this.Logger = logger;
            this.Name = "DataPurge";
            string msg = string.Format("Started {0} Update Frequency = {1}", this.Name, this.Frequency);
            Logger.Info(msg);

        }

        public void Process()
        {
            try
            {
                IsProcessing = true;
                string msg = string.Format("DataPurge Process Next Purge Time = {0} Current = {1}", this.NextPurge, DateTime.UtcNow);
                this.Logger.Debug(msg);
                Console.WriteLine(msg);
                if (NextPurge <= DateTime.UtcNow)
                {
                    var config = this.Repo.GetPurgeConfiguration();
                    if (config != null)
                    {
                        foreach (var table in config)
                        {
                            bool foundItemsToDeleteForTable = false;
                            msg = string.Format("Processing Table {0} With Threshold of {1} Days, Id = {2}", table.Name, table.Threshold, table.Id);
                            Logger.Debug(msg);
                            Console.WriteLine(msg);
                            Logger.Info(msg);
                            bool continueProcessing = true;
                            while(continueProcessing)
                            {
                                    List<RowItem> rowsToDelete = Repo.GetItemsToDelete(table.Name, table.Threshold, 2500);
                                    continueProcessing = false;
                                    if (rowsToDelete.Any())
                                    {
                                        continueProcessing = true;
                                        foundItemsToDeleteForTable = true;
                                        List<long> ids = rowsToDelete.Select(x => x.Id).ToList();
                                        msg = string.Format(" Table {0} Found {1} Rows over {2} days old", table.Name, ids.Count, table.Threshold);
                                        Console.WriteLine(msg);
                                        Logger.Info(msg);
                                        msg = "Deleting Items";
                                        Console.WriteLine(msg);
                                        Logger.Info(msg);
                                        this.Repo.DeleteItems(table.Name, ids);
                                    }
                            }

                            if(foundItemsToDeleteForTable)
                            {
                                msg = "Optimizing Table";
                                Console.WriteLine(msg);
                                Logger.Info(msg);
                                this.Repo.OptimizeTable(table.Name);
                            }

                        }
                        NextPurge = DateTime.UtcNow + this.Frequency;
                        //Console.WriteLine(msg);

                        // TODO Delete
                        msg = string.Format("Next UTC Purge Time = {0}", this.NextPurge);
                        Console.WriteLine(msg);
                        Logger.Info(msg);


                    }
                }
            }
          finally
            {
                IsProcessing = false;
            }
          
        }

        public void Shutdown()
        {
            this.Repo = null;
        }
    }
}
