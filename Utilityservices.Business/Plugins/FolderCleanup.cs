﻿using NLog;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilityservices.Business;
using UtilityServices.Models;
using UtilityServices.Repository;
using System.Diagnostics;
using System.Security.Principal;

namespace UtilityServices.Business.Plugins
{
    [Export(typeof(IPlugin))]
    public class FolderCleanup : IPlugin
    {
        public bool IsProcessing { get; set; }
        private Repo Repo { get; set; }
        public string Name { get; set; }
        public NLog.Logger Logger { get; set; }
        public bool Enabled { get; set; }
        public string InputShare { get; set; }
        public string OutputShare { get; private set; }
        public string Nas1InputShare { get; set; }
        public string Nas1OutputShare { get; private set; }
        public string Nas2InputShare { get; set; }
        public string Nas2OutputShare { get; private set; }
        public string Nas3InputShare { get; set; }
        public string Nas3OutputShare { get; private set; }
        public string TempFolder1 { get; private set; }
        public string TempFolder2 { get; private set; }
        public String CommandLineArg { get; set; }
        public long currentInstitutionID { get; set; }
        public bool isNas { get; set; }

        public FolderCleanup()
        {

        }
        public void Initialize(string connectionString, NameValueCollection configuration, Logger logger)
        {
            try
            {
                using (Process p = new Process())
                {
                    string filename = @"E:\Apps\utilityservices - Institutions\reconnect.bat";

                    string ocrexepath = filename.Replace(@"/", @"\");
                    string allArguments = "";
                    p.StartInfo.FileName = ocrexepath;
                    p.StartInfo.Arguments = allArguments;
                    p.StartInfo.UseShellExecute = false;
                    p.Start();
                    p.WaitForExit();
                    p.Close();
                }
            }
            catch (Exception ex)
            {

            }

            IsProcessing = false;
            this.CommandLineArg = "f";
            this.Repo = new Repo(connectionString);

            string enabled = configuration["FolderCleanupEnabled"];
            if (!string.IsNullOrEmpty(enabled))
            {
                this.Enabled = Convert.ToBoolean(enabled);
            }

            this.InputShare = configuration["InputFolder"] ?? string.Empty;
            this.OutputShare = configuration["OutputFolder"] ?? string.Empty;
            this.Nas1InputShare = configuration["Nas1InputFolder"] ?? string.Empty;
            this.Nas1OutputShare = configuration["Nas1OutputFolder"] ?? string.Empty;
            this.Nas2InputShare = configuration["Nas2InputFolder"] ?? string.Empty;
            this.Nas2OutputShare = configuration["Nas2OutputFolder"] ?? string.Empty;
            this.Nas3InputShare = configuration["Nas3InputFolder"] ?? string.Empty;
            this.Nas3OutputShare = configuration["Nas3OutputFolder"] ?? string.Empty;
            this.TempFolder1 = configuration["TempFolder1"] ?? string.Empty;
            this.TempFolder2 = configuration["TempFolder2"] ?? string.Empty;

            this.Logger = logger;
            this.Name = "FolderCleanup";
            string msg = string.Format("Started {0} Time = {1}, Input Share = {2}, Output Share = {3}", this.Name, DateTime.UtcNow, this.InputShare, this.OutputShare);
            Logger.Info(msg);
        }

        public void Process()
        {

       
           try
            {
                IsProcessing = true;

                //X:\data\input\HUBERT_ROBERT____20190404_170540008_done
                //DirectoryInfo di1 = new DirectoryInfo("X:\\data\\input\\HUBERT_ROBERT____20190404_170540008_done");
                //DirectoryInfo di2 = new DirectoryInfo("M:\\data\\input\\HUBERT_ROBERT____20190404_170540008_done");
                //DirectoryInfo di1 = new DirectoryInfo("X:\\data\\input\\COOPER__LILLIAN_20170821_120458124_done");
                //DirectoryInfo di2 = new DirectoryInfo("\\\\TS1400R902\\ChimpStorage\\data\\input");
                //DirectoryInfo di3 = new DirectoryInfo("Z:\\chimp\\core");

                string msg = string.Format("{0} StartTime = {1}", this.Name, DateTime.UtcNow);
                this.Logger.Debug(msg);
                Console.WriteLine(msg);
                if (this.Enabled)
                {
                    long previousStudyId = 0;
                    bool continueReading = true;
                    long maxId = 0;
                    List<FolderInfo> folders = new List<FolderInfo>();
                    Dictionary<long, bool> finishedStudyIds = new Dictionary<long, bool>();
                    while (continueReading)
                    {
                        if (folders.Any())
                        {
                            maxId = folders.Last().ImageId;
                        }
                        Dictionary<string, bool> outputPathsProcessed = new Dictionary<string, bool>();
                        Dictionary<string, bool> nas1OutputPathsProcessed = new Dictionary<string, bool>();
                        Dictionary<string, bool> nas2OutputPathsProcessed = new Dictionary<string, bool>();
                        Dictionary<string, bool> nas3OutputPathsProcessed = new Dictionary<string, bool>();

                        folders = this.Repo.GetFoldersToDelete(2500, maxId);
                        continueReading = folders != null && folders.Any();
                        foreach (var folder in folders)
                        {
                            currentInstitutionID = folder.InstId;

                            if (folder.StudyId != previousStudyId)
                            {
                                if (previousStudyId > 0)
                                {
                                    if (!finishedStudyIds.ContainsKey(previousStudyId))
                                    {
                                        Repo.UpdateStudyFinished(previousStudyId);
                                        finishedStudyIds.Add(previousStudyId, true);
                                    }
                                }
                                previousStudyId = folder.StudyId;
                            }
                            // Generate Full Path to shared folders
                            folder.Nas1InputPath = string.Format("{0}\\{1}_done", this.Nas1InputShare, folder.InputPath);
                            folder.Nas2InputPath = string.Format("{0}\\{1}_done", this.Nas2InputShare, folder.InputPath);
                            folder.Nas3InputPath = string.Format("{0}\\{1}_done", this.Nas3InputShare, folder.InputPath);
                            folder.InputPath = string.Format("{0}\\{1}_done", this.InputShare, folder.InputPath);

                            if (!string.IsNullOrWhiteSpace(folder.OutputPath))
                            {
                                folder.Nas1OutputPath = string.Format("{0}\\{1}", this.Nas1OutputShare, folder.OutputPath);
                                folder.Nas2OutputPath = string.Format("{0}\\{1}", this.Nas2OutputShare, folder.OutputPath);
                                folder.Nas3OutputPath = string.Format("{0}\\{1}", this.Nas3OutputShare, folder.OutputPath);
                                folder.OutputPath = string.Format("{0}\\{1}", this.OutputShare, folder.OutputPath);
                                if (!outputPathsProcessed.ContainsKey(folder.OutputPath))
                                {
                                    isNas = false;
                                    DirectoryInfo diOutput = new DirectoryInfo(folder.OutputPath);
                                    msg = string.Format("{0} Processing Folder Output Path = {1}, InstId = {2} With Threshold of {3} Days, Age = {4}, ImageId = {5}", this.Name, folder.OutputPath, folder.InstId, folder.Threshold, folder.Age, folder.ImageId);
                                    Logger.Info(msg);
                                    Console.WriteLine(msg);
                                    if (diOutput.Exists)
                                        TraverseDirectory(diOutput);
                                    outputPathsProcessed.Add(folder.OutputPath, true);
                                }
                                if (!nas1OutputPathsProcessed.ContainsKey(folder.Nas1OutputPath))
                                {
                                    isNas = true;
                                    DirectoryInfo diOutput = new DirectoryInfo(folder.Nas1OutputPath);
                                    msg = string.Format("{0} Processing NAS 1 Folder Output Path = {1}, InstId = {2} With Threshold of {3} Days, Age = {4}, ImageId = {5}", this.Name, folder.OutputPath, folder.InstId, folder.Threshold, folder.Age, folder.ImageId);
                                    Logger.Info(msg);
                                    Console.WriteLine(msg);
                                    if (diOutput.Exists)
                                        TraverseDirectory(diOutput);
                                    nas1OutputPathsProcessed.Add(folder.Nas1OutputPath, true);
                                }
                                if (!nas2OutputPathsProcessed.ContainsKey(folder.Nas2OutputPath))
                                {
                                    isNas = true;
                                    DirectoryInfo diOutput = new DirectoryInfo(folder.Nas2OutputPath);
                                    msg = string.Format("{0} Processing NAS 2 Folder Output Path = {1}, InstId = {2} With Threshold of {3} Days, Age = {4}, ImageId = {5}", this.Name, folder.OutputPath, folder.InstId, folder.Threshold, folder.Age, folder.ImageId);
                                    Logger.Info(msg);
                                    Console.WriteLine(msg);
                                    if (diOutput.Exists)
                                        TraverseDirectory(diOutput);
                                    nas2OutputPathsProcessed.Add(folder.Nas2OutputPath, true);
                                }
                                if (!nas3OutputPathsProcessed.ContainsKey(folder.Nas3OutputPath))
                                {
                                    isNas = true;
                                    DirectoryInfo diOutput = new DirectoryInfo(folder.Nas2OutputPath);
                                    msg = string.Format("{0} Processing NAS 3 Folder Output Path = {1}, InstId = {2} With Threshold of {3} Days, Age = {4}, ImageId = {5}", this.Name, folder.OutputPath, folder.InstId, folder.Threshold, folder.Age, folder.ImageId);
                                    Logger.Info(msg);
                                    Console.WriteLine(msg);
                                    if (diOutput.Exists)
                                        TraverseDirectory(diOutput);
                                    nas3OutputPathsProcessed.Add(folder.Nas3OutputPath, true);
                                }
                            }

                            isNas = false;
                            DirectoryInfo diInput = new DirectoryInfo(folder.InputPath);
                            msg = string.Format("{0} Processing Folder Input Path = {1}, InstId = {2} With Threshold of {3} Days, Age = {4}, ImageId = {5}", this.Name, folder.InputPath, folder.InstId, folder.Threshold, folder.Age, folder.ImageId);
                            Logger.Info(msg);
                            Console.WriteLine(msg);
                            if (diInput.Exists)
                                TraverseDirectory(diInput);

                            isNas = true;
                            diInput = new DirectoryInfo(folder.Nas1InputPath);
                            msg = string.Format("{0} Processing Nas 1 Folder Input Path = {1}, InstId = {2} With Threshold of {3} Days, Age = {4}, ImageId = {5}", this.Name, folder.InputPath, folder.InstId, folder.Threshold, folder.Age, folder.ImageId);
                            Logger.Info(msg);
                            Console.WriteLine(msg);
                            if (diInput.Exists)
                                TraverseDirectory(diInput);

                            isNas = true;
                            diInput = new DirectoryInfo(folder.Nas2InputPath);
                            msg = string.Format("{0} Processing Nas 2 Folder Input Path = {1}, InstId = {2} With Threshold of {3} Days, Age = {4}, ImageId = {5}", this.Name, folder.InputPath, folder.InstId, folder.Threshold, folder.Age, folder.ImageId);
                            Logger.Info(msg);
                            Console.WriteLine(msg);
                            if (diInput.Exists)
                                TraverseDirectory(diInput);

                            isNas = true;
                            diInput = new DirectoryInfo(folder.Nas3InputPath);
                            msg = string.Format("{0} Processing Nas 3 Folder Input Path = {1}, InstId = {2} With Threshold of {3} Days, Age = {4}, ImageId = {5}", this.Name, folder.InputPath, folder.InstId, folder.Threshold, folder.Age, folder.ImageId);
                            Logger.Info(msg);
                            Console.WriteLine(msg);
                            if (diInput.Exists)
                                TraverseDirectory(diInput);
                        }

                    }

                    // Cover the case of the last one or we never had more than one study id
                    if (previousStudyId > 0 && !finishedStudyIds.ContainsKey(previousStudyId))
                    {
                        Repo.UpdateStudyFinished(previousStudyId);
                        finishedStudyIds.Add(previousStudyId, true);
                    }

                    msg = string.Format("{0} EndTime = {1} ", this.Name, DateTime.UtcNow);
                    Console.WriteLine(msg);
                    Logger.Info(msg);
                }
            }
            catch(Exception ex)
            { }
            finally
            {
                IsProcessing = false;
                this.Repo = null;
            }
        }

        private void ProcessTempFolder(string tempName, string tempPath)
        {
            string msg;
            // Process Temp Configured folders and delete their contents:
            if (!string.IsNullOrEmpty(tempPath))
            {
                DirectoryInfo diTemp = new DirectoryInfo(tempPath);
                if (diTemp.Exists)
                {
                    msg = string.Format("{0} Temp Folder {1} Exists = {2} ", this.Name, tempName, tempPath);
                    Console.WriteLine(msg);
                    Logger.Info(msg);
                    msg = string.Format("{0} Processing Temp Folder = {0} ", this.Name, tempPath);
                    Console.WriteLine(msg);
                    Logger.Info(msg);
                    TraverseParentDirectory(diTemp);
                }
                else
                {
                    msg = string.Format("{0} Temp Folder {1} DOES NOT EXIST = {2} ", this.Name, tempName, tempPath);
                    Console.WriteLine(msg);
                    Logger.Error(msg);
                }

            }
        }
        private void TraverseParentDirectory(DirectoryInfo di)
        {
            di.Refresh();
            foreach (var subDir in di.GetDirectories())
            {
                DateTime dt = Directory.GetCreationTime(subDir.FullName.ToString());

                if (DateTime.Today.Subtract(dt).TotalDays > 3)
                    TraverseDirectory(subDir);
            }
        }
        private void TraverseDirectory(DirectoryInfo di)
        {
            di.Refresh();
            foreach (var subDir in di.GetDirectories())
            {
                TraverseDirectory(subDir);
            }

            CleanAllFilesInDirectory(di);
            string msg = string.Format("{0} Deleting Folder {1}", this.Name, di.FullName);
            Console.WriteLine(msg);
            Logger.Info(msg);
            di.Delete();
        }

        private void CleanAllFilesInDirectory(DirectoryInfo di)
        {
            foreach(var file in di.GetFiles())
            {
                Repo.AddAuditRecord(currentInstitutionID, file.Name, file.Length, isNas ? 1 : 0);
                if (file.IsReadOnly)
                    file.IsReadOnly = false;
                file.Delete();
                string msg = string.Format("{0} Deleted File {1}", this.Name, file.FullName);
                Console.WriteLine(msg);
                Logger.Info(msg);
            }
        }

        public void Shutdown()
        {
            this.Repo = null;
        }
    }
}
